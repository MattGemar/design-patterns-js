class MovieCard {
	constructor(movie) {
		this._movie = movie;
	}

	createMovieCard() {
		const $wrapper = document.createElement('dic');
		$wrapper.classList.add('movie-card-wrapper');

		const movieCard = `
            <div class="movie-thumbnail center">
	            <img
		            src="${this._movie.picture}"
		            alt="${this._movie.title}"
	            />
            </div>
            <h3 class="fs-16 center">${this._movie.title}</h3>
            <p class="fs-14 center">
            <span>${this._movie.released_in}</span>
            <span>${this._movie.duration}</span>
            </p>
        `;

		$wrapper.innerHTML = movieCard;
		return $wrapper;
	}
}
