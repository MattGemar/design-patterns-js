class Api {
	/*
	 *
	 * @param {string} url
	 *
	 */
	constructor(url) {
		this._url = url;
	}

	async get() {
		return fetch(this._url)
			.then((res) => res.json())
			.then((res) => res.data)
			.catch((err) => console.log('Une erreur est survenue', err));
	}
}

class MovieApi extends Api {
	/*
	 *
	 * @param {string} url
	 *
	 */
	constructor(url) {
		super(url);
	}

	async getMovies() {
		return await this.get();
	}
}
