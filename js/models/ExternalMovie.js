class ExternalMovie {
	constructor(data) {
		this._medias = data.medias;
		this._infos = data.infos;
		this._synopsis = data.synopsis;
		this._title_fr = data.title_fr;
		this._title_en = data.title_en;
	}

	get title() {
		return this._title_fr ? this._title_fr : this._title_en;
	}

	get picture() {
		return `/assets/${this._medias.picture}`;
	}

	get thumbnail() {
		return `/assets/thumbnails/${this._medias.thumbnail}`;
	}

	get synopsis() {
		return this._synopsis;
	}

	get duration() {
		return this._infos.duration;
	}

	get released_in() {
		return this._infos.released_in;
	}
}
